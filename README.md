<p align="center">
  <a href="https://tinyuidesign.cloudbu.huawei.com/" target="_blank" rel="noopener noreferrer">
    <img alt="TinyVue Logo" src="./logo.svg" height="100" style="max-width:100%;">
  </a>
</p>

<p align="center">Tiny-Vue-Mobile 是 TinyVue 组件库的移动端组件库 </p>


## 本地开发
```shell
# 安装依赖
pnpm i
# 初始化官网套件
pnpm preSite

```

## 开源协议

[MIT](LICENSE)